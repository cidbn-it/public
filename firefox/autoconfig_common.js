// Functions of AutoConfig
// The following functions are available within an AutoConfig file:
// pref(prefName, value) – sets the user value of a preference. This function explicitly sets the preference as a user preference. That means that if the user has changed the value, it will get reset every time the browser is started.
// defaultPref(prefName, value) – sets the default value of a preference. This is the value that a preference has when the user has not set any value or the user resets the preference. It can be permanently changed by the user.
// lockPref(prefName, value) – sets the default value of a preference and locks it. This is the most commonly used function. Locking a preference prevents a user from changing it, and in most cases, disables the UI in preferences so it is obvious to the user that the preference has been disabled.
// unlockPref(prefName) – unlocks a preference. As an example, this would be used in case where a preference should be locked for all users, but unlocked for particular users.
// getPref(prefName) – retrieves the value of a preference. If the preference doesn’t exist, it displays an error. This function should only be used on preferences that always exist.
// clearPref(prefName) – removes the user value of a preference, resetting it to its default value.
// displayError(funcname, message) – displays an error in a specific format, which is a handy tool for debugging.
//    Netscape.cfg/AutoConfig failed. Please contact your system administrator.
//    Error: [funcname] failed: [message]
// getenv(name) – used to query environment variables. This can allow access to things like usernames and other system information.

//defaultPref("pref.privacy.disable_button.view_passwords",true);
//defaultPref("identity.fxaccounts.enabled",false);                               // Disable FirefoxSync Feature
defaultPref("security.secure_connection_icon_color_gray", false);
defaultPref("dom.event.contextmenu.enabled", false);                            // Don't alllow website to block right click
defaultPref("signon.rememberSignons", false);                                   // Don't ask and remember credentials
defaultPref("browser.toolbars.bookmarks.visibility", "always");
defaultPref("browser.sessionstore.warnOnQuit", false);                          // Don't warn when quitting browser
defaultPref("browser.startup.page", 3);                                         // Restore previous version
defaultPref("browser.search.widget.inNavBar", true);

// Telemetry
pref("devtools.onboarding.telemetry.logged", false);
pref("toolkit.telemetry.updatePing.enabled", false);
pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
pref("browser.newtabpage.activity-stream.telemetry", false);
pref("browser.ping-centre.telemetry", false);
pref("toolkit.telemetry.bhrPing.enabled", false);
pref("toolkit.telemetry.enabled", false);
pref("toolkit.telemetry.firstShutdownPing.enabled", false);
pref("toolkit.telemetry.hybridContent.enabled", false);
pref("toolkit.telemetry.newProfilePing.enabled", false);
pref("toolkit.telemetry.reportingpolicy.firstRun", false);
pref("toolkit.telemetry.unified", false);
pref("toolkit.telemetry.shutdownPingSender.enabled", false);
pref("toolkit.telemetry.updatePing.enabled", false);
pref("toolkit.telemetry.reportingpolicy.firstRun", false);
pref("toolkit.telemetry.unified", false);
pref("toolkit.telemetry.archive.enabled", false);
pref("devtools.onboarding.telemetry.logged", false);
pref("toolkit.telemetry.bhrPing.enabled", false);
pref("datareporting.healthreport.uploadEnabled", false);
pref("datareporting.policy.dataSubmissionEnabled", false);
pref("datareporting.sessions.current.clean", true);
pref("datareporting.healthreport.uploadEnabled", false);
pref("datareporting.policy.dataSubmissionEnabled", false);
pref("datareporting.sessions.current.clean", true);

pref("privacy.donottrackheader.enabled", true);
